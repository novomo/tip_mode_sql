const tableBuilder = require("../../sql_table_builder/builder");
const IP = require("ip");
const upload_error = require("../../../node_error_functions/upload_error");

module.exports = async (session, db) => {
  try {
    const tipster_cols = {
      columns: [
        {
          label: "tipsterID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "url", type: "VARCHAR(150)" },
        { label: "profilePic", type: "VARCHAR(150)" },
        { label: "lastUpdated", type: "INT" },
        { label: "platform", type: "VARCHAR(50)" },
        { label: "handle", type: "VARCHAR(50)" },
        { label: "stakingFactor", type: "DOUBLE" },
        { label: "fakeCount", type: "INT" },
        { label: "numOfTips", type: "INT" },
        { label: "tipsterSkatingFactorID", type: "VARCHAR(50)" },
      ],
      primary_key: "PRIMARY KEY (tipsterID)",
    };

    const tipster_profile_total_cols = {
      columns: [
        {
          label: "tipsterProfileTotalID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "tipsterID", type: "INT(11) UNSIGNED" },
        { label: "category", type: "VARCHAR(150)" },
        { label: "ROI", type: "DOUBLE" },
        { label: "lastUpdated", type: "INT" },
        { label: "market", type: "VARCHAR(50)" },
        { label: "unitsRisked", type: "DOUBLE" },
        { label: "profileMade", type: "DOUBLE" },
      ],
      primary_key: "PRIMARY KEY (tipsterProfileTotalID)",
      foreign_key: "FOREIGN KEY (tipsterID) REFERENCES tipsters (tipsterID)",
    };

    const tipster_profile_cols = {
      columns: [
        {
          label: "tipsterProfileID",
          type: "INT(11) UNSIGNED NOT NULL AUTO_INCREMENT",
        },
        { label: "tipsterID", type: "INT(11) UNSIGNED" },
        { label: "periodWeeks", type: "INT" },
        { label: "category", type: "VARCHAR(150)" },
        { label: "ROI", type: "DOUBLE" },
        { label: "lastUpdated", type: "INT" },
        { label: "market", type: "VARCHAR(50)" },
        { label: "unitsRisked", type: "DOUBLE" },
        { label: "profileMade", type: "DOUBLE" },
      ],
      primary_key: "PRIMARY KEY (tipsterProfileID)",
      foreign_key: "FOREIGN KEY (tipsterID) REFERENCES tipsters (tipsterID)",
    };

    await tableBuilder(tipster_cols, "in4freedom", "tipsters", session);
    await tableBuilder(
      tipster_profile_cols,
      "in4freedom",
      "tipster_current_profiles",
      session
    );
    await tableBuilder(
      tipster_profile_total_cols,
      "in4freedom",
      "tipster_profiles",
      session
    );

    /* {
    url:String
      starttime: int,
      runners: [{
        horseID: int,
        jockeyID: int,
        position: int,
        gate: int,
        equipment: string
        CDWinner: Boolean
      }],
      course: {
        courseID: int!
        distance : Float
      raceType: String
      ground: String
      courseType: String
      }

    } */
    await db.createCollection("tips", {
      reuseExisting: true,
    });

    await db.createCollection("tipster_staking_factors", {
      reuseExisting: true,
    });
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Updating bet",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
